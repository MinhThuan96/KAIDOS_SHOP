﻿using Model.Dao;
using Model.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace KaiDoShop.Areas.Admin.Controllers
{
    public class ContentController 
    {
        private Kaidoshop db = new Kaidoshop();

        // GET: Admin/Content
        public ActionResult Index(string searchString, int page = 1, int pageSize = 10)
        {
            var dao = new ContentDao(); 
            var model = dao.ListAllPaging(searchString, page, pageSize);
            ViewBag.SearchString = searchString;
            return View(model);
        }

        //Phương thức tạo tin tức mới
        [HttpGet]
        public ActionResult Create()
        {
            SetViewBag();
            return View();
        }

        //Phương thức tạo tin tức mới
        [HttpGet]
        public ActionResult Edit(long id)
        {
            var dao = new ContentDao();
            var content = dao.GetByID(id);            
            SetViewBag(content.CategoryID);
            return View(content);
        }

        //Phương thức cập nhật tin tức mới
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Content model)
        {

            if (ModelState.IsValid) //Kiểm lỗi bên phía server
            {
                var dao = new ContentDao();
                var result = dao.Update(model);
                if (result)
                {
                    SetAlert("Cap nhat tin tuc thanh cong", "succsess");
                    return RedirectToAction("Index", "Content");
                }
                else
                {
                    ModelState.AddModelError("", "Cap nhat tin tuc khong thanh cong");
                }
                return RedirectToAction("Index");
            }
            SetViewBag(model.CategoryID);
            return View();
        }

        [HttpPost]
        [ValidateInput(false)] //Xác nhận đầu vào
        public ActionResult Create(Content model)
        {
            if (ModelState.IsValid)
            {
                new ContentDao().Create(model);
                return RedirectToAction("Index");
            }
            SetViewBag(model.CategoryID);
            return View();
        }

        //Phương thức xóa Content
        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new ContentDao().Delete(id);
            return RedirectToAction("Index");
        }

        public void SetViewBag(long? selectedId = null)
        {
            var dao = new CategoryDao();
            ViewBag.CategoryID = new SelectList(dao.ListAll(), "ID", "Name", selectedId);
        }

        //Phương thức thay đổi Status

        public JsonResult ChangeStatuss(long id)
        {
            var result = new UserDao().ChangeStatuss(id);
            return Json(new {statuss = result});
        }
    }
}