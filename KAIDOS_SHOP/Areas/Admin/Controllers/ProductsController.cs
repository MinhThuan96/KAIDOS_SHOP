﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.EF;
using PagedList;
using PagedList.Mvc;
using Model.Dao;

namespace KaiDoShop.Areas.Admin.Controllers
{
    public class ProductsController : Controller
    {
        private Kaidoshop db = new Kaidoshop();

        // GET: Admin/Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        //Phương thức đánh dấu trang sản phẩm
        public ActionResult sanpham(int? page)
        {
            int pageNumber = (page ?? 1);
            int pageSize = 7;
            return View(db.Products.ToList().OrderBy(n => n.ID).ToPagedList(pageNumber, pageSize));
        }     

        // GET: Admin/Products/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Admin/Products/Create
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Code,MetaTilte,Desription,Image,MoreImages,Price,PromotionPrice,IncludeVat,Quantity,CategoryID,Detal,Warranty,CreateDate,CreateBy,ModifeDate,ModifeBy,MetaKeywords,MeteDesriptions,Status,TopHot,ViewCount")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("sanpham");
            }
            return View(product);
        }

        // GET: Admin/Products/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Product product = db.Products.Find(id);
            var product = db.Products.FirstOrDefault(m=>m.ID == id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Code,MetaTilte,Desription,Image,MoreImages,Price,PromotionPrice,IncludeVat,Quantity,CategoryID,Detal,Warranty,CreateDate,CreateBy,ModifeDate,ModifeBy,MetaKeywords,MeteDesriptions,Status,TopHot,ViewCount")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("sanpham");
            }
            return View(product);
        }

        // GET: Admin/Products/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Product product = db.Products.Find(id);
            var product = db.Products.FirstOrDefault(m=>m.ID ==id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Admin/Products/Delete/
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            //Product product = db.Products.Find(id);
            var product = db.Products.FirstOrDefault(m=>m.ID ==id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("sanpham");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
