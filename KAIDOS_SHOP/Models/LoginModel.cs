﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaiDoShop.Models
{
    public class LoginModel
    {
        [Key]
        [Display(Name ="Tên Đăng Nhập")]
        [Required(ErrorMessage ="Bạn Phải Nhập Tài Khoản")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "Bạn Phải Nhập Mật Khẩu")]
        [Display(Name = "Mật khẩu")]   
        public string Password { set; get; }

    }
}