﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace KaiDoShop.Models
{
    //Phương thức đăng kí người dùng và các thông báo lỗi khi bỏ trống
    public class RegisterModel
    {
        [Key]
        public long ID { set; get; }
        [Display(Name="Tên Đăng Nhập")]
        [Required(ErrorMessage ="Yêu Cầu Nhập Tên Đăng Nhập")]
        public string UserName { set; get; }
        [StringLength(20,MinimumLength =6,ErrorMessage ="Độ dài mật khẩu ít nhất 6 ký tự")]
        [Required(ErrorMessage = "Yêu Cầu Nhập Mật Khẩu")]
        [Display(Name = "Mật khẩu")]
        public string Password { set; get; }
        [Display(Name = "Xác Nhận Mật Khẩu")]
        [Compare("Password",ErrorMessage ="Xác nhận mật khẩu không đúng. ")]
        
        public string ConfirmPassword { set; get; }
        [Required(ErrorMessage = "Yêu Cầu Nhập Họ Tên")]
        [Display(Name = "Họ Tên")]
        public string Name { set; get; }
        [Display(Name = "Địa chỉ")]
        public string Address { set; get; }
        [Required(ErrorMessage = "Yêu cầu nhập họ tên")]
        [Display(Name = "Email")]
        public string Email { set; get; }
        [Display(Name = "Điện thoại")]
        public string Phone { set; get; }
    }
}